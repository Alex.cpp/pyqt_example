import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")
    ColumnLayout {
        spacing: 20
        anchors.centerIn: parent.center
        Button{
            text: "Нажми!"
            onClicked: {
                lblText.text = "Hello PyQt!"
            }
        }
        Label {
            id: lblText
            text: ""
        }
    }
}
